﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class EmpleadoModel
    {
        public static List<Empleado> Empleados = new List<Empleado>();


        public static List<Empleado> GetEmpleados()
        {
            return Empleados;
        }

        public static void Populate()
        {
            Empleado[] Emp =
            {
                new Empleado(1,"001-4509","Carlos","Rocha","89765432","En Tipitapa","Administrador de Bodega",Empleado.Sexo.Masculino,20000.50),
                new Empleado(2,"001-3227","Rufino","Flores","67893645","Altamira del este","Gerente general",Empleado.Sexo.Masculino,40000.0),
                new Empleado(3,"001-5638","Ana","Silva","87689796","En san Judas","Gerente de marketing",Empleado.Sexo.Femenino,25000.0)
            };

            Empleados = Emp.ToList();

        }

    }
}
