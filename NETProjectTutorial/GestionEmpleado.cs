﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class GestionEmpleado : Form
    {
        public DataSet dsEmpleado;
        public BindingSource bsEmpleado;
        public GestionEmpleado()
        {
            InitializeComponent();
            bsEmpleado = new BindingSource();
        }

        public DataSet DsEmpleado {set { dsEmpleado = value; } }

        private void TxtFinder_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bsEmpleado.Filter = string.Format("codigo like '*{0}*' or Nombre like '*{0}*' or Apellido like '*{0}*' or Utilidad like '*{0}*' or Telefono like '*{0}*' ", txtFinder.Text);
            }catch(InvalidExpressionException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void GestionEmpleado_Load(object sender, EventArgs e)
        {
            bsEmpleado.DataSource = dsEmpleado;
            bsEmpleado.DataMember = dsEmpleado.Tables["Empleado"].TableName;
            dataGridView1.DataSource = bsEmpleado;
            dataGridView1.AutoGenerateColumns = true;
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            FrmEmpleado fe = new FrmEmpleado();
            fe.TblEmpleados = dsEmpleado.Tables["Empleado"];
            fe.DsEmpleados = dsEmpleado;
            fe.ShowDialog();

        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            Empleado emp = (Empleado)dataGridView1.CurrentRow.DataBoundItem;
        }
    }
}
