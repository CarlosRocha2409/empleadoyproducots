﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class Empleado
    {
        private int id;
        private String Nombre, Apellido, Telefono,Direccion, Utilidad,codigo;
        private Sexo sexo;
        private double Salario;

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Nombre1
        {
            get
            {
                return Nombre;
            }

            set
            {
                Nombre = value;
            }
        }

        public string Apellido1
        {
            get
            {
                return Apellido;
            }

            set
            {
                Apellido = value;
            }
        }

        public string Telefono1
        {
            get
            {
                return Telefono;
            }

            set
            {
                Telefono = value;
            }
        }

        public string Direccion1
        {
            get
            {
                return Direccion;
            }

            set
            {
                Direccion = value;
            }
        }

        public string Utilidad1
        {
            get
            {
                return Utilidad;
            }

            set
            {
                Utilidad = value;
            }
        }

        public string Codigo
        {
            get
            {
                return codigo;
            }

            set
            {
                codigo = value;
            }
        }

        internal Sexo Sexo1
        {
            get
            {
                return sexo;
            }

            set
            {
                sexo = value;
            }
        }

        public double Salario1
        {
            get
            {
                return Salario;
            }

            set
            {
                Salario = value;
            }
        }

        public enum Sexo {Masculino,Femenino}

       
        public Empleado() { }


        public Empleado(int id, string codigo, string nombre, string apellido, string telefono, string direccion, string utilidad, Sexo sexo, double salario)
        {
            this.Id = id;
            Nombre1 = nombre;
            Apellido1 = apellido;
            Telefono1 = telefono;
            Direccion1 = direccion;
            Utilidad1 = utilidad;
            this.Codigo = codigo;
            this.Sexo1 = sexo;
            Salario1 = salario;
        }
    }
}
