﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NETProjectTutorial.entities;

namespace NETProjectTutorial
{
    public partial class FrmEmpleado : Form
    {
        private DataTable tblEmpleados;
        private DataSet dsEmpleados;
        private BindingSource bsEmpleados;
        public FrmEmpleado()
        {
            InitializeComponent();
            bsEmpleados = new BindingSource();
        }

        public DataTable TblEmpleados { set { tblEmpleados = value; } }
        public DataSet DsEmpleados { set { dsEmpleados = value; } }

        private void FrmEmpleado_Load(object sender, EventArgs e)
        {
            bsEmpleados.DataSource = dsEmpleados;
            bsEmpleados.DataMember = dsEmpleados.Tables["Empleado"].TableName;

            bindingNavigator2.BindingSource = bsEmpleados;
            

            txtCodigo.DataBindings.Add("Text", bsEmpleados, "Codigo");
            txtNombre.DataBindings.Add("Text", bsEmpleados, "Nombre");
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            String N = txtNombre.Text;
            String C = txtCodigo.Text;
            String A = txtApellido.Text;
            String U = txtUtilidad.Text;
            String T = msktxtTelefono.Text;
            String D = txtDireccion.Text;
            Double S;
            Empleado.Sexo sex;

            bool result = Double.TryParse(msktxtSlario.Text, out S);

            if (cmbSexo.SelectedIndex == 0)
            {
                sex = Empleado.Sexo.Femenino;
            }
            else
            {
                sex = Empleado.Sexo.Masculino;
            }

            tblEmpleados.Rows.Add(tblEmpleados.Rows.Count + 1, C, N, A,T,D, U,sex, S);
        }

        
    }
}
