﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NETProjectTutorial.Properties;

namespace NETProjectTutorial
{
    public partial class FrmProducto : Form
    {
        private DataTable tblProductos ;
        private DataSet dsProducto;
        private BindingSource bsProducto;    

        public FrmProducto()
        {
            InitializeComponent();
            bsProducto = new BindingSource();
        }

        public DataTable TblProductos { set { tblProductos = value; } }

        public DataSet DsProducto{ set { dsProducto = value;}}

        private void btnSave_Click(object sender, EventArgs e)
        {
            string sku, nombres, descripcion;
            int cantidad;
            double precio;

            sku = txtSku.Text;
            nombres = txtName.Text;
            descripcion = txtDesc.Text;
            cantidad = (int)nmQuant.Value;
            bool result = Double.TryParse(msktxtPri.Text, out precio);

            tblProductos.Rows.Add(tblProductos.Rows.Count + 1, sku, nombres, descripcion, cantidad, precio);
        }

        private void FrmProducto_Load(object sender, EventArgs e)
        {
            bsProducto.DataSource = dsProducto;
            bsProducto.DataMember = dsProducto.Tables["Producto"].TableName;

            bindingNavigator1.BindingSource = bsProducto;

            txtSku.DataBindings.Add("Text", bsProducto, "Sku");
            txtName.DataBindings.Add("Text", bsProducto, "Nombre");
        }

        
    }
}
