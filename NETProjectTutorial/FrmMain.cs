﻿using NETProjectTutorial.entities;
using NETProjectTutorial.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmMain : Form
    {
        private DataTable dtProductos;
        private DataTable dtEmpleados;

        public FrmMain()
        {
            InitializeComponent();
        }

        private void productosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionProductos fgp = new FrmGestionProductos();
            fgp.MdiParent = this;
            fgp.DsProductos = dsProductos;
            fgp.Show();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            dtProductos = dsProductos.Tables["Producto"];
            dtEmpleados = dsProductos.Tables["Empleado"];
            ProductoModel.Populate();
            EmpleadoModel.Populate();
            foreach (Producto p in ProductoModel.GetProductos())
            {
                dtProductos.Rows.Add(p.Id, p.Sku, p.Nombre, p.Descripcion, p.Cantidad, p.Precio);
            }
            foreach (Empleado E in EmpleadoModel.GetEmpleados())
            {
                dtEmpleados.Rows.Add(E.Id,E.Codigo,E.Nombre1,E.Apellido1,E.Telefono1,E.Direccion1,E.Utilidad1,E.Sexo1,E.Salario1);
            }
        }

        private void EmpleadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GestionEmpleado fge = new GestionEmpleado();
            fge.MdiParent = this;
            fge.DsEmpleado = dsProductos;
            fge.Show();
        }
    }
}
